package the_fireplace.unlogic.blocks;

import the_fireplace.unlogic.unbase;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.IBlockAccess;

public class PortalFrameBlock extends WBlock{

	public PortalFrameBlock(Material p_i45394_1_) {
		super(p_i45394_1_);
		setCreativeTab(unbase.TabUnLogic);
	}
	@Override
	public boolean isBeaconBase(IBlockAccess worldObj, int x, int y, int z, int beaconX, int beaconY, int beaconZ)
    {
        return true;
    }
	
}

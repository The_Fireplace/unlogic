package the_fireplace.unlogic.blocks;

import java.util.Random;

import the_fireplace.unlogic.unbase;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockCactus;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;

public class StrippedCactus extends BlockCactus implements IPlantable {
    @SideOnly(Side.CLIENT)
    private IIcon field_150038_a;
    @SideOnly(Side.CLIENT)
    private IIcon field_150037_b;

	public StrippedCactus() {
		super();
        this.setTickRandomly(true);
        this.setCreativeTab(unbase.TabUnLogic);
	}
	@Override
	public void updateTick(World world, int x, int y, int z, Random rand)
    {
        if (world.isAirBlock(x, y + 1, z))
        {
            int l;

            for (l = 1; world.getBlock(x, y - l, z) == this; ++l)
            {
                ;
            }

            if (l < 3)
            {
                int i1 = world.getBlockMetadata(x, y, z);

                if (i1 == 15)
                {
                	world.setBlock(x, y + 1, z, this);
                	world.setBlockMetadataWithNotify(x, y, z, 0, 4);
                    this.onNeighborBlockChange(world, x, y + 1, z, this);
                }
                else
                {
                	world.setBlockMetadataWithNotify(x, y, z, i1 + 1, 4);
                }
            }
        }
    }
	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z)
    {
        float f = 0.0625F;
        return AxisAlignedBB.getBoundingBox((double)((float)x + f), (double)y, (double)((float)z + f), (double)((float)(x + 1) - f), (double)((float)(y + 1) - f), (double)((float)(z + 1) - f));
    }
	@Override
	@SideOnly(Side.CLIENT)
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int x, int y, int z)
    {
        float f = 0.0625F;
        return AxisAlignedBB.getBoundingBox((double)((float)x + f), (double)y, (double)((float)z + f), (double)((float)(x + 1) - f), (double)(y + 1), (double)((float)(z + 1) - f));
    }
	@Override
	public boolean renderAsNormalBlock()
    {
        return false;
    }
	@Override
	public boolean isOpaqueCube()
    {
        return false;
    }
	@Override
	public int getRenderType()
    {
        return 13;
    }
	@Override
	public EnumPlantType getPlantType(IBlockAccess world, int x, int y, int z) {
		return EnumPlantType.Desert;
	}
	@Override
	public boolean canPlaceBlockAt(World p_149742_1_, int p_149742_2_, int p_149742_3_, int p_149742_4_)
    {
        return !super.canPlaceBlockAt(p_149742_1_, p_149742_2_, p_149742_3_, p_149742_4_) ? false : this.canBlockStay(p_149742_1_, p_149742_2_, p_149742_3_, p_149742_4_);
    }

	@Override
	public Block getPlant(IBlockAccess world, int x, int y, int z) {
		return this;
	}

	@Override
	public int getPlantMetadata(IBlockAccess world, int x, int y, int z) {
		return -1;
	}
	@Override
	public void onEntityCollidedWithBlock(World p_149670_1_, int p_149670_2_, int p_149670_3_, int p_149670_4_, Entity p_149670_5_){}
	
	//begin texture code
	@Override
	@SideOnly(Side.CLIENT)
    public IIcon getIcon(int p_149691_1_, int p_149691_2_)
    {
        return p_149691_1_ == 1 ? this.field_150038_a : (p_149691_1_ == 0 ? this.field_150037_b : this.blockIcon);
    }
	@Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister p_149651_1_)
    {
        this.blockIcon = p_149651_1_.registerIcon("unlogic:strippedcactus_side");
        this.field_150038_a = p_149651_1_.registerIcon("unlogic:strippedcactus_top");
        this.field_150037_b = p_149651_1_.registerIcon("cactus_bottom");
    }
}

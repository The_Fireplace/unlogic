package the_fireplace.unlogic.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.World;

public class KnowledgeBlock extends Block{

	public KnowledgeBlock(Material p_i45394_1_) {
		super(p_i45394_1_);
		setBlockName("KnowledgeBlock");
		setBlockTextureName("unlogic:knowledgeBlock");
	}
	@Override
	public float getEnchantPowerBonus(World world, int x, int y, int z)
	{
		return 3;
	}
}

package the_fireplace.unlogic.containers;

import the_fireplace.unlogic.entities.tile.TileEntityDepletionFurnace;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;

public class ContainerDepletionFurnace extends Container {
    private int lastCookTime;
    private int lastBurnTime;
    private int lastItemBurnTime;
    private TileEntityDepletionFurnace tileFurnace;

    public ContainerDepletionFurnace(InventoryPlayer inv, TileEntityDepletionFurnace tile){
    	this.tileFurnace = tile;
    	//this.addSlotToContainer(new Slot(tile, slotIndex, x, y));//for reference. slotIndex is which slot it binds to(the int of the fuel slot, input, etc)
    	this.addSlotToContainer(new Slot(tile, 0, 56, 17));//Should be the input
    	this.addSlotToContainer(new Slot(tile, 0, 56, 53));//Should be the 'fuel'
    }
	@Override
	public boolean canInteractWith(EntityPlayer p_75145_1_) {
		return false;
	}

}

package the_fireplace.unlogic.entities;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import the_fireplace.unlogic.unbase;

public class EntityUnlogicGem extends EntityItem {
	int transformTime = 0;
    /** The health of this EntityItem. (For example, damage for tools) */
    private int health;
	public EntityUnlogicGem(World world){
		super(world);
        this.health = 5;
        this.hoverStart = (float)(Math.random() * Math.PI * 2.0D);
        this.setSize(0.25F, 0.25F);
        this.yOffset = this.height / 2.0F;
        this.delayBeforeCanPickup = 50;
	}
	public EntityUnlogicGem(World world, double x,
			double y, double z, ItemStack is) {
		super(world, x, y, z, is);        
        this.setEntityItemStack(is);
        this.lifespan = (is.getItem() == null ? 6000 : is.getItem().getEntityLifespan(is, world));
        this.delayBeforeCanPickup = 50;
	}
	public EntityUnlogicGem(World world, double x, double y, double z){
		super(world);
        this.health = 5;
        this.hoverStart = (float)(Math.random() * Math.PI * 2.0D);
        this.setSize(0.25F, 0.25F);
        this.yOffset = this.height / 2.0F;
        this.setPosition(x, y, z);
        this.rotationYaw = (float)(Math.random() * 360.0D);/*
        this.motionX = (double)((float)(Math.random() * 0.20000000298023224D - 0.10000000149011612D));
        this.motionY = 0.20000000298023224D;
        this.motionZ = (double)((float)(Math.random() * 0.20000000298023224D - 0.10000000149011612D));*/
        this.delayBeforeCanPickup = 50;
	}
	@Override
	public void onUpdate()
	{
		super.onUpdate();
		int j = MathHelper.floor_double( this.posX );
		int i = MathHelper.floor_double( this.posY );
		int k = MathHelper.floor_double( this.posZ );
		transformTime++;
		if ( transformTime > 60 )
		{
			if ( !transform() )
				transformTime = 0;
		}
	}
	public static boolean isSameItem(ItemStack ol, ItemStack op)
	{
		return ol != null && op != null && ol.isItemEqual( op );
	}
	public boolean transform()
	{
		World world = this.worldObj;
		if(!world.isRemote){
		ItemStack item = getEntityItem();
			AxisAlignedBB region = AxisAlignedBB.getBoundingBox( posX - 1, posY - 1, posZ - 1, posX + 1, posY + 1, posZ + 1 );
			List<Entity> l = worldObj.getEntitiesWithinAABBExcludingEntity( this, region );

			EntityItem iron = null;

			for (Entity e : l)
			{
				if ( e instanceof EntityItem && !e.isDead )
				{
					ItemStack other = ((EntityItem) e).getEntityItem();
					if ( other != null && other.stackSize > 0 )
					{
						if ( isSameItem( other, new ItemStack( Items.iron_ingot ) ) )
							iron = (EntityItem) e;
					}
				}
			}

			if ( iron != null )
			{
				iron.getEntityItem().stackSize--;

				if ( iron.getEntityItem().stackSize <= 0 )
					iron.setDead();

				ItemStack Output = new ItemStack(unbase.ReactiveIron);
				worldObj.spawnEntityInWorld( new EntityItem( worldObj, posX, posY, posZ, Output ) );

				return true;
			}}
		
		return false;
	}
}

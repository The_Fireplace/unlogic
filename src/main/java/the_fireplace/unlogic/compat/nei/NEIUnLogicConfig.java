package the_fireplace.unlogic.compat.nei;

import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import the_fireplace.unlogic.unbase;
import the_fireplace.unlogic.config.UnLogicConfigValues;
import codechicken.nei.api.API;
import codechicken.nei.api.IConfigureNEI;
import codechicken.nei.api.ItemInfo;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Optional;

@Optional.Interface(iface = "codechicken.nei.api.API", modid = "NotEnoughItems")
public class NEIUnLogicConfig implements IConfigureNEI {

	@Optional.Method(modid="NotEnoughItems")
	@Override
	public String getName() {
		return unbase.class.getAnnotation(Mod.class).name();
	}

	@Optional.Method(modid="NotEnoughItems")
	@Override
	public String getVersion() {
		return unbase.class.getAnnotation(Mod.class).version();
	}

	@Optional.Method(modid="NotEnoughItems")
	@Override
	public void loadConfig() {
		API.hideItem(new ItemStack(unbase.BlazeCake));
		API.hideItem(new ItemStack(unbase.ChocolateCake));
		API.hideItem(new ItemStack(unbase.QCrop, 1, OreDictionary.WILDCARD_VALUE));
		API.hideItem(new ItemStack(unbase.DepletionFurnaceOn));
		//Deprecated Content
		API.hideItem(new ItemStack(unbase.RedGem));
		API.hideItem(new ItemStack(unbase.BlueGem));
		API.hideItem(new ItemStack(unbase.RedGemBlock));
		API.hideItem(new ItemStack(unbase.BlueGemBlock));
		API.hideItem(new ItemStack(unbase.LBlackDye));
		API.hideItem(new ItemStack(unbase.LRedDye));
		API.hideItem(new ItemStack(unbase.LGreenDye));
		API.hideItem(new ItemStack(unbase.LSilverDye));
		API.hideItem(new ItemStack(unbase.LBlueDye));
		API.hideItem(new ItemStack(unbase.LGreyDye));
		API.hideItem(new ItemStack(unbase.LBrownDye));
		API.hideItem(new ItemStack(unbase.LLimeDye));
		API.hideItem(new ItemStack(unbase.LOrangeDye));
		API.hideItem(new ItemStack(unbase.LWhiteDye));
		API.hideItem(new ItemStack(unbase.LTrueWhiteDye));
		API.hideItem(new ItemStack(unbase.LPinkDye));
		API.hideItem(new ItemStack(unbase.LMagentaDye));
		API.hideItem(new ItemStack(unbase.LCyanDye));
		API.hideItem(new ItemStack(unbase.LSkyDye));
		API.hideItem(new ItemStack(unbase.LPurpleDye));
		API.hideItem(new ItemStack(unbase.LYellowDye));
		//Incomplete/WIP content
		if(!(UnLogicConfigValues.DEVKEY == unbase.DEVKEY)){
		API.hideItem(new ItemStack(unbase.FarmObsidian));
		API.hideItem(new ItemStack(unbase.UnLogicStone));
		API.hideItem(new ItemStack(unbase.MSTable));
		API.hideItem(new ItemStack(unbase.LightConducter));
		API.hideItem(new ItemStack(unbase.DepletionFurnace));
		API.hideItem(new ItemStack(unbase.AppleGlasses));//Overpriced Glasses
		API.hideItem(new ItemStack(unbase.PortalFrame));
		}
	}

}

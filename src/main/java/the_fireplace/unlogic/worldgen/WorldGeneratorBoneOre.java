package the_fireplace.unlogic.worldgen;

import java.util.Random;

import the_fireplace.unlogic.unbase;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import cpw.mods.fml.common.IWorldGenerator;

public class WorldGeneratorBoneOre implements IWorldGenerator {
	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world,
	IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
	switch(world.provider.dimensionId){
	case 0 : generateSurface(world, random,chunkX*16,chunkZ*16);
	}
	}

	private void generateSurface(World world, Random random, int BlockX, int BlockZ) {
	for(int i =0; i<50;i++){
	int Xcoord = BlockX + random.nextInt(16);
	int Zcoord = BlockZ + random.nextInt(16);
	int Ycoord = random.nextInt(16);
	(new WorldGenMinable(unbase.BoneOre, 3)).generate(world, random, Xcoord, Ycoord, Zcoord);
	}}
}

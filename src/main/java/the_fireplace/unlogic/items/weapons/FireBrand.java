package the_fireplace.unlogic.items.weapons;

import the_fireplace.unlogic.unbase;
import net.minecraft.item.ItemSword;

public class FireBrand extends ItemSword {

	public FireBrand(ToolMaterial p_i45356_1_) {
		super(p_i45356_1_);
		setUnlocalizedName("FireBrand");
		setTextureName("unlogic:fire_brand");
		setCreativeTab(unbase.TabUnLogic);
	}

}

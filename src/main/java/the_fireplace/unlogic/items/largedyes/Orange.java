package the_fireplace.unlogic.items.largedyes;

import java.util.List;

import the_fireplace.unlogic.unbase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Orange extends Item{
	public Orange(){
		setCreativeTab(unbase.TabUnLogic);
		setTextureName("unlogic:orange");
		setUnlocalizedName("dyeorange");
	}
	@Override
	public void addInformation(ItemStack itemStack, EntityPlayer player,
            List list, boolean par4) {
		list.add("DEPRECATED ITEM, USE BY UNLOGIC 1.1.0.0!");
	}
}

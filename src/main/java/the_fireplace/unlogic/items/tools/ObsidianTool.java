package the_fireplace.unlogic.items.tools;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import the_fireplace.unlogic.unbase;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;

public class ObsidianTool extends Item {
	public ObsidianTool(){
		this.setMaxStackSize(1);
		this.setUnlocalizedName("ObsidianTool");
		this.setTextureName("unlogic:obsidian_tool");
		this.setCreativeTab(unbase.TabUnLogic);
		this.setMaxDamage(1562);//same as diamond pickaxe
		//this.setRepairItem(Items.diamond);
	}
	//On Item Use on Obsidian, turn it in to Farm Obsidian
	/**
     * Current implementations of this method in child classes do not use the entry argument beside ev. They just raise
     * the damage on the stack.
     */
	@Override
    public boolean hitEntity(ItemStack p_77644_1_, EntityLivingBase p_77644_2_, EntityLivingBase p_77644_3_)
    {
        p_77644_1_.damageItem(2, p_77644_3_);
        return true;
    }
    @Override
    public boolean onBlockDestroyed(ItemStack is, World world, Block block, int x, int y, int z, EntityLivingBase entity)
    {
        if ((double)block.getBlockHardness(world, x, y, z) != 0.0D)
        {
            is.damageItem(1, entity);
        }
        if(block == Blocks.obsidian || block == unbase.FarmObsidian){
        	block.dropBlockAsItem(world, x, y, z, 1, 0);//1 is the amount; 0 is the metadata
        }

        return true;
    }

    /**
     * Returns True is the item is renderer in full 3D when hold.
     */
    @Override
    @SideOnly(Side.CLIENT)
    public boolean isFull3D()
    {
        return true;
    }
    /**
     * Return the enchantability factor of the item, most of the time is based on material.
     */
    @Override
    public int getItemEnchantability()
    {
    	return 10;
    }
    @Override
    public boolean getIsRepairable(ItemStack p_82789_1_, ItemStack p_82789_2_)
    {
        return true;
    }
    @Override
    public float getDigSpeed(ItemStack stack, Block block, int meta)
    {
        if (block == Blocks.obsidian || block == unbase.FarmObsidian)
        {
            return 5000;
        }
        return super.getDigSpeed(stack, block, meta);
    }
}

package the_fireplace.unlogic.items.components;

import the_fireplace.unlogic.unbase;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;

public class EndPiece extends Item{
	public EndPiece(){
		setCreativeTab(unbase.TabUnLogic);
		setUnlocalizedName("EndPiece");
		setTextureName("unlogic:endPiece");
	}
}

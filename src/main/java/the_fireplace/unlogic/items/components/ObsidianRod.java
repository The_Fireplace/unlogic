package the_fireplace.unlogic.items.components;

import the_fireplace.unlogic.unbase;
import net.minecraft.item.Item;

public class ObsidianRod extends Item {
	public ObsidianRod(){
		setTextureName("unlogic:ObsidianRod");
		setUnlocalizedName("ObsidianRod");
		setCreativeTab(unbase.TabUnLogic);
	}

}

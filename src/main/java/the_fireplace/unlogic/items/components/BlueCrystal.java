package the_fireplace.unlogic.items.components;

import java.util.List;

import the_fireplace.unlogic.unbase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class BlueCrystal extends Item{
	public BlueCrystal(){
		setUnlocalizedName("UnlogicBlueCrystal");
	}
	@Override
	public void addInformation(ItemStack itemStack, EntityPlayer player,
            List list, boolean par4) {
		list.add("DEPRECATED ITEM, USE BY UNLOGIC 1.2.0.0!");
	}
}
